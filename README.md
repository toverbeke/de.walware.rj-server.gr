# RJSrv for GNU R

RJSrv (R Server for RJ) implementation for GNU R.

## Installation

The server is provided as R packages.

For installation instructions see wiki page [Installation](https://gitlab.com/walware/de.walware.rj-server.gr/wikis/Installation).
