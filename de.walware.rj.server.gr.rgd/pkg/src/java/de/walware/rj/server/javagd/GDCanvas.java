//
//  GDCanvas.java
//  JavaGD - Java Graphics Device for R
//
//  Copyright (c) 2004, 2019 Simon Urbanek. All rights reserved.
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation;
//  version 2.1 of the License.
//  
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//  
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Simon Urbanek <simon.urbanek@r-project.org> - initial API and implementation (org.rosuda.javagd)
// Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation for RJ
// Stephan Wahlbrink <sw@wahlbrink.eu> - added support for canvas color
//

package de.walware.rj.server.javagd;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.reflect.Method;
import java.util.Vector;


public class GDCanvas extends Canvas implements GDContainer, MouseListener {
    Vector l;

    boolean listChanged;
    
    public static boolean forceAntiAliasing=true;

    GDState gs;
    
    Refresher r;
    
    Dimension lastSize;

    public int devNr=-1;
    
    public GDCanvas(double w, double h, int canvasColor) {
        this((int)w, (int)h, canvasColor);
    }
    
    public GDCanvas(int w, int h, int canvasColor) {
        this.l=new Vector();
        this.gs=new GDState();
        this.gs.f=new Font(null,0,12);
        setSize(w,h);
        this.lastSize=getSize();
        setBackground(new Color((canvasColor & 255), ((canvasColor>>8) & 255), ((canvasColor>>16) & 255)));
	addMouseListener(this);
        (this.r=new Refresher(this)).start();
    }

    @Override
	public GDState getGState() { return this.gs; }

    @Override
	public void setDeviceNumber(int dn) { this.devNr=dn; }
    @Override
	public int getDeviceNumber() { return this.devNr; }
    @Override
	public void closeDisplay() {}
    
    public synchronized void cleanup() {
        this.r.active=false;
        this.r.interrupt();
        reset();
        this.r=null;
        this.l=null;
    }

    @Override
	public void syncDisplay(boolean finish) {
        repaint();
    }
    
    public void initRefresh() {
        //System.out.println("resize requested");
        try { // for now we use no cache - just pure reflection API for: Rengine.getMainEngine().eval("...")
            Class c=Class.forName("org.rosuda.JRI.Rengine");
            if (c==null) {
				System.out.println(">> can't find Rengine, automatic resizing disabled. [c=null]");
			}
			else {
                Method m=c.getMethod("getMainEngine",null);
                Object o=m.invoke(null,null);
                if (o!=null) {
                    Class[] par=new Class[1];
                    par[0]=Class.forName("java.lang.String");
                    m=c.getMethod("eval",par);
                    Object[] pars=new Object[1];
                    pars[0]="try(.Call(\"javaGDresize\", "+this.devNr+"L)), PACKAGE= \"rj.gd\", silent= TRUE)";
                    m.invoke(o, pars);
                }
            }
        } catch (Exception e) {
            System.out.println(">> can't find Rengine, automatic resizing disabled. [x:"+e.getMessage()+"]");
        }
    }
    
    @Override
	public synchronized void add(GDObject o) {
        this.l.add(o);
        this.listChanged=true;
    }
    
    @Override
	public synchronized void reset() {
        this.l.removeAllElements();
        this.listChanged=true;
    }

    LocatorSync lsCallback=null;

    @Override
	public synchronized boolean prepareLocator(LocatorSync ls) {
	if (this.lsCallback!=null && this.lsCallback!=ls) {
		this.lsCallback.triggerAction(null);
	}
	this.lsCallback=ls;
	
	return true;
    }

    // MouseListener for the Locator support
    @Override
	public void mouseClicked(MouseEvent e) {
	if (this.lsCallback!=null) {
	    double[] pos = null;
	    if ((e.getModifiers()&InputEvent.BUTTON1_MASK)>0 && (e.getModifiers()&(InputEvent.BUTTON2_MASK|InputEvent.BUTTON3_MASK))==0) { // B1 = return position
		pos = new double[2];
		pos[0] = e.getX();
		pos[1] = e.getY();
	    }

	    // pure security measure to make sure the trigger doesn't mess with the locator sync object
	    LocatorSync ls=this.lsCallback;
	    this.lsCallback=null; // reset the callback - we'll get a new one if necessary
	    ls.triggerAction(pos);
	}
    }

    @Override
	public void mousePressed(MouseEvent e) {}
    @Override
	public void mouseReleased(MouseEvent e) {}
    @Override
	public void mouseEntered(MouseEvent e) {}
    @Override
	public void mouseExited(MouseEvent e) {}


    public synchronized Vector getGDOList() { return this.l; }

    long lastUpdate;
    long lastUpdateFinished;
    boolean updatePending=false;
    
    @Override
	public void update(Graphics g) {
        if (System.currentTimeMillis()-this.lastUpdate<200) {
            this.updatePending=true;
            if (System.currentTimeMillis()-this.lastUpdateFinished>700) {
                g.setColor(Color.white);
                g.fillRect(0,0,250,25);
                g.setColor(Color.blue);
                g.drawString("Building plot... ("+this.l.size()+" objects)",10,10);
                this.lastUpdateFinished=System.currentTimeMillis();
            }
            this.lastUpdate=System.currentTimeMillis();
            return;
        }
        this.updatePending=false;
        super.update(g);
        this.lastUpdateFinished=this.lastUpdate=System.currentTimeMillis();
    }

    class Refresher extends Thread {
        GDCanvas c;
        boolean active;

        public Refresher(GDCanvas c) {
            this.c=c;
        }

        @Override
		public void run() {
            this.active=true;
            while (this.active) {
                try {
                    Thread.sleep(300);
                } catch (Exception e) {}
                if (!this.active) {
					break;
				}
                if (this.c.updatePending && (System.currentTimeMillis()-GDCanvas.this.lastUpdate>200)) {
                    this.c.repaint();
                }
            }
            this.c=null;
        }
    }
    
    @Override
	public synchronized void paint(Graphics g) {
        this.updatePending=false;
        Dimension d=getSize();
        if (!d.equals(this.lastSize)) {
            initRefresh();
            this.lastSize=d;
            return;
        }

        if (forceAntiAliasing) {
            Graphics2D g2=(Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }
               
        int i=0, j=this.l.size();
        g.setFont(this.gs.f);
        g.setClip(0,0,d.width,d.height); // reset clipping rect
        while (i<j) {
            GDObject o=(GDObject) this.l.elementAt(i++);
            o.paint(this, this.gs, g);
        }
        this.lastUpdate=System.currentTimeMillis();
    }
}
