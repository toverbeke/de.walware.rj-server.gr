//
//  JavaGD.java - default GDInterface implementation for use in JavaGD
//  JavaGD - Java Graphics Device for R
// 
//  Copyright (c) 2004, 2019  Simon Urbanek
// 
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation;
//  version 2.1 of the License.
//  
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//  
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Simon Urbanek <simon.urbanek@r-project.org> - initial API and implementation (org.rosuda.javagd)
// Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation for RJ
// Stephan Wahlbrink <sw@wahlbrink.eu> - added support for canvas color
//

package de.walware.rj.server.javagd;

import java.awt.Frame;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;


/** JavaGD is an implementation of the {@link GDInterface} protocol which displays the R graphics in an AWT window (via {@link GDCanvas}). It can be used as an example gfor implementing custom display classes which can then be used by JavaGD. Three sample back-ends are included in the JavaGD sources: {@link GDCanvas} (AWT), {@link JGDPanel} (Swing) and {@link JGDBufferedPanel} (Swing with cached update). */
public class JavaGD extends GDContainerGD implements WindowListener {
    /** frame containing the graphics canvas */ 
    public Frame f;
    
    /** default, public constructor - creates a new JavaGD instance. The actual window (and canvas) is not created until {@link #gdOpen} is called. */
    public JavaGD() {
        super();
    }
    
    @Override
    public void     gdOpen(int devNr) {
        super.gdOpen(devNr);
        if (this.f!=null) {
			gdClose();
		}

        this.f=new Frame("JavaGD ("+(getDeviceNumber()+1)+")"+(isActive()?" *active*":""));
        this.f.addWindowListener(this);
        this.c=new GDCanvas(getWidth(), getHeight(), getCanvasColor());
        this.f.add((GDCanvas)this.c);
        this.f.pack();
        this.f.setVisible(true);
    }

    @Override
    public void     gdActivate() {
        super.gdActivate();
        if (this.f!=null) {
            this.f.requestFocus();
            this.f.setTitle("JavaGD ("+(getDeviceNumber()+1)+")"+" *active*");
        }
    }

    @Override
    public void     gdClose() {
        super.gdClose();
        if (this.f!=null) {
            this.c=null;
            this.f.removeAll();
            this.f.dispose();
            this.f=null;
        }
    }

    @Override
    public void     gdDeactivate() {
        super.gdDeactivate();
        if (this.f!=null) {
			this.f.setTitle("JavaGD ("+(getDeviceNumber()+1)+")");
		}
    }

    @Override
    public void     gdNewPage() {
        super.gdNewPage();
    }

    /*-- WindowListener interface methods */
    
    /** listener response to "Close" - effectively invokes <code>dev.off()</code> on the device */
    @Override
	public void windowClosing(WindowEvent e) {
        if (this.c!=null) {
			executeDevOff();
		}
    }
    @Override
	public void windowClosed(WindowEvent e) {}
    @Override
	public void windowOpened(WindowEvent e) {}
    @Override
	public void windowIconified(WindowEvent e) {}
    @Override
	public void windowDeiconified(WindowEvent e) {}
    @Override
	public void windowActivated(WindowEvent e) {}
    @Override
	public void windowDeactivated(WindowEvent e) {}
    
}
