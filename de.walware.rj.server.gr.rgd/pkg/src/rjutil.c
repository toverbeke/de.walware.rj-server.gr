/*=============================================================================#
 # Copyright (c) 2011, 2019 Stephan Wahlbrink and others.
 # All rights reserved. This program and the accompanying materials
 # are made available under the terms of the Apache License v2.0
 # which accompanies this distribution, and is available at
 # https://www.apache.org/licenses/LICENSE-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

#include <R_ext/Error.h>

#include "rjutil.h"


static int rjinit= 0;

static JavaVM *jVM;

static jclass jcClass= 0;
static jclass jcRJClassLoader= 0;
static jmethodID jmClassForName= 0;
static jobject joRJClassLoader= 0;
static jmethodID jmRJClassLoaderAddClassPath= 0;


static void rj_init(JNIEnv *env) {
	jclass jc;
	jmethodID jm;
	jobject jo;
	
	(*env)->GetJavaVM(env, &jVM);
	
	const char *errorMessage= "Failed to init Java classloader support (%s).";
	
	jc= (*env)->FindClass(env, "java/lang/Class");
	if (!jc) {
		rjinit= -1;
		handleJError(env, RJ_ERROR_RERROR, errorMessage, "finding class 'java.lang.Class'");
	}
	jcClass= (*env)->NewGlobalRef(env, jc);
	if (!jcClass) {
		rjinit= -1;
		handleJError(env, RJ_ERROR_RERROR, errorMessage, "creating ref for 'jcClass'");
	}
	(*env)->DeleteLocalRef(env, jc);
	
	jm= (*env)->GetStaticMethodID(env, jcClass, "forName", "(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;");
	if (!jm) {
		rjinit= -1;
		handleJError(env, RJ_ERROR_RERROR, errorMessage, "getting method 'java.lang.Class#forName(String, boolean, ClassLoader)'");
	}
	jmClassForName= jm;
	
	jc= (*env)->FindClass(env, "RJavaClassLoader");
	if (!jc) {
		rjinit= -2;
		handleJError(env, RJ_ERROR_RERROR, errorMessage, "finding class 'RJavaClassLoader'");
	}
	jcRJClassLoader= (*env)->NewGlobalRef(env, jc);
	if (!jcRJClassLoader) {
		rjinit= -1;
		handleJError(env, RJ_ERROR_RERROR, errorMessage, "creating ref for 'jcRJClassLoader'");
	}
	(*env)->DeleteLocalRef(env, jc);
	
	jm= (*env)->GetStaticMethodID(env, jcRJClassLoader, "getPrimaryLoader", "()LRJavaClassLoader;");
	if (!jm) {
		rjinit= -1;
		handleJError(env, RJ_ERROR_RERROR, errorMessage, "getting static method 'RJavaClassLoader#getPrimaryLoader()'");
	}
	jo= (*env)->CallStaticObjectMethod(env, jcRJClassLoader, jm);
	if (!jo) {
		rjinit= -1;
		handleJError(env, RJ_ERROR_RERROR, errorMessage, "calling 'RJavaClassLoader#getPrimaryLoader()'");
	}
	joRJClassLoader= (*env)->NewGlobalRef(env, jo);
	if (!joRJClassLoader) {
		rjinit= -1;
		handleJError(env, RJ_ERROR_RERROR, errorMessage, "creating ref for 'joRJavaClassLoader'");
	}
	(*env)->DeleteLocalRef(env, jo);
	
	jm= (*env)->GetMethodID(env, jcRJClassLoader, "addClassPath", "(Ljava/lang/String;)V");
	if (!jm) {
		rjinit= -1;
		handleJError(env, RJ_ERROR_RERROR, errorMessage, "getting method 'RJavaClassLoader#addClassPath(String)'");
	}
	jmRJClassLoaderAddClassPath= jm;
	
	rjinit= 1;
}

void initJUtil(JNIEnv *env) {
	if (rjinit != 1) {
		rj_init(env);
	}
}

JavaVM *getJVM() {
	return jVM;
}

JNIEnv *getJEnv() {
	JNIEnv *env;
	jint rc;
	
	if (!jVM) {
		jsize n;
		if ((rc= JNI_GetCreatedJavaVMs(&jVM, 1, &n)) != JNI_OK) {
			handleJError(0, RJ_ERROR_CERROR, "Failed to get Java VM (return.code= %d).", (int) rc);
			return 0;
		}
		if (n < 1) {
			return 0;
		}
	}
	
	if ((rc= (*jVM)->AttachCurrentThread(jVM, (void**) &env, 0)) != JNI_OK) {
		handleJError(0, RJ_ERROR_CERROR, "Failed to attach thread to Java VM (return.code= %d).", (int) rc);
		return 0;
	}
	return env;
}

void addJClassPath(JNIEnv *env, const char *path) {
	if (rjinit != 1) {
		rj_init(env);
	}
	
	jstring jPath= newJString(env, path, RJ_ERROR_RERROR);
	(*env)->CallObjectMethod(env, joRJClassLoader, jmRJClassLoaderAddClassPath, jPath);
	(*env)->DeleteLocalRef(env, jPath);
}

jstring newJString(JNIEnv *env, const char *s, int flags) {
	jstring js= (*env)->NewStringUTF(env, s);
	if (!js) {
		handleJError(env, flags, "Failed to create new Java string '%s'.", s);
		return 0;
	}
	
	if ((flags & RJ_GLOBAL_REF) == RJ_GLOBAL_REF) {
		jstring global= (*env)->NewGlobalRef(env, js);
		(*env)->DeleteLocalRef(env, js);
		if (!global) {
			handleJError(env, flags, "Failed to create ref for Java string '%s'.", s);
			return 0;
		}
		return global;
	}
	else {
		return js;
	}
}


jclass getJClass(JNIEnv *env, const char *name, int flags) {
	jclass jc;
	
	if (rjinit != 1) {
		rj_init(env);
	}
	
	{	jstring js= newJString(env, name, flags);
		if (!js) {
			return 0;
		}
		jc= (*env)->CallStaticObjectMethod(env, jcClass, jmClassForName, js, JNI_TRUE,
				joRJClassLoader );
		(*env)->DeleteLocalRef(env, js);
	}
	if (!jc) {
		handleJError(env, flags, "Failed to get Java class '%s'.", name);
		return 0;
	}
	
	if ((flags & RJ_GLOBAL_REF) == RJ_GLOBAL_REF) {
		jclass global= (*env)->NewGlobalRef(env, jc);
		(*env)->DeleteLocalRef(env, jc);
		if (!global) {
			handleJError(env, flags, "Failed to create ref for Java class '%s'.", name);
			return 0;
		}
		return global;
	}
	else {
		return jc;
	}
}

jmethodID getJMethod(JNIEnv *env, jclass jClass, const char *name, const char *sig, int flags) {
	jmethodID jm;
	
	if (rjinit != 1) {
		rj_init(env);
	}
	
	jm= (*env)->GetMethodID(env, jClass, name, sig);
	if (!jm) {
		handleJError(env, flags, "Failed to get Java method '%s'.", name);
		return 0;
	}
	
	return jm;
}


void handleJError(JNIEnv *env, int flags, const char *message, ...) {
	if (env && (*env)->ExceptionCheck(env) == JNI_TRUE) {
#ifdef JGD_DEBUG
		(*env)->ExceptionDescribe(env);
#endif
		if (!(flags & 0x7)) {
			return;
		}
		
		(*env)->ExceptionClear(env);
	}
	
	if ((flags & (0x2 | 0x4))) {
		va_list ap;
		char msg[1024];
		msg[1023]= 0;
		
		va_start(ap, message);
		vsnprintf(msg, 1023, message, ap);
		va_end(ap);
		
		switch (flags & 0xf) {
		case RJ_ERROR_RWARNING:
			Rf_warning("[RJ-RSrv/JNI] %s", msg);
			break;
		case RJ_ERROR_RERROR:
			Rf_error("[RJ-RSrv/JNI] %s", msg);
			break;
		default:
			fprintf(stderr, "[RJ-RSrv/JNI] %s\n", msg);
			fflush(stderr);
			break;
		}
	}
}

void handleJNewArrayError(JNIEnv *env, const char *operation) {
	handleJError(env, RJ_ERROR_RERROR, "%s: Failed to create Java array.", operation);
}

void handleJGetArrayError(JNIEnv *env, jobject jArray, const char *operation) {
	if (jArray) {
		(*env)->DeleteLocalRef(env, jArray);
	}
	handleJError(env, RJ_ERROR_RERROR, "%s: Failed to access Java array.", operation);
}

void handleJNewStringError(JNIEnv *env, const char *operation) {
	handleJError(env, RJ_ERROR_RERROR, "%s: Failed to create Java string.", operation);
}
