/*=============================================================================#
 # Copyright (c) 2011, 2019 Stephan Wahlbrink and others.
 # All rights reserved. This program and the accompanying materials
 # are made available under the terms of the Apache License v2.0
 # which accompanies this distribution, and is available at
 # https://www.apache.org/licenses/LICENSE-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

// Utility methods for R package libs using JNI with R-Java classloader (from JRI or compatible)


#ifndef RJUTIL_H
#define RJUTIL_H

#include <Rdefines.h>
#include <jni.h>


#define CHAR_UTF8(s) (Rf_getCharCE(s) == CE_UTF8) ? CHAR(s) : Rf_reEnc(CHAR(s), Rf_getCharCE(s), CE_UTF8, 1)

#define RJ_ERROR_SWALLOW 0x1
#define RJ_ERROR_RWARNING 0x2
#define RJ_ERROR_RERROR 0x3
#define RJ_ERROR_CERROR 0x5
#define RJ_GLOBAL_REF 0x10


void initJUtil(JNIEnv *env);
JavaVM *getJVM();
JNIEnv *getJEnv();

void addJClassPath(JNIEnv *env, const char *path);

jstring newJString(JNIEnv *env, const char *s, int flags);

jclass getJClass(JNIEnv *env, const char *name, int flags);
jmethodID getJMethod(JNIEnv *env, jclass jClass, const char *name, const char *sig, int flags);

void handleJError(JNIEnv *env, int flags, const char *message, ...);

void handleJNewArrayError(JNIEnv *env, const char *operation);
void handleJGetArrayError(JNIEnv *env, jobject jArray, const char *operation);
void handleJNewStringError(JNIEnv *env, const char *operation);


#endif
