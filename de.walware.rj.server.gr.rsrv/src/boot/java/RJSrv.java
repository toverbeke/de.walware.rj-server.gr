/*=============================================================================#
 # Copyright (c) 2018 Stephan Wahlbrink and others.
 # All rights reserved. This program and the accompanying materials
 # are made available under the terms of the  Apache License v2.0
 # which accompanies this distribution, and is available at
 # https://www.apache.org/licenses/LICENSE-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public class RJSrv {
	
	
	private static final Pattern PATH_SPLITTER_PATTERN= Pattern.compile(":,:", Pattern.LITERAL);
	
	private static final String RJ_CLASSPATH_PROPERTY_KEY= "org.eclipse.statet.rj.server.ClassPath.urls"; //$NON-NLS-1$
	
	
	private static String getUrlByClassloader(final Class<?> refClass) {
		Exception cause= null;
		final String resourceName= refClass.getName().replace('.', '/') + ".class"; //$NON-NLS-1$
		final URL url= refClass.getClassLoader().getResource(resourceName);
		try {
			String s= url.toURI().toString();
			if (s.endsWith(resourceName)) {
				s= s.substring(0, s.length() - resourceName.length());
				if (s.startsWith("jar:") && s.endsWith("!/")) { //$NON-NLS-1$ //$NON-NLS-2$
					s= s.substring(4, s.length() - 2);
				}
				return s;
			}
		}
		catch (final URISyntaxException e) {
			cause= e;
		}
		throw new UnsupportedOperationException("url= " + url, cause);
	}
	
	public static URI getPkgLib(final String name) throws URISyntaxException {
		String url= null;
		final String refUrl= getUrlByClassloader(RJSrv.class);
		int idx;
		if ((idx= refUrl.lastIndexOf("boot")) >= 0) {
			url= refUrl.substring(0, idx) +
					name +
					refUrl.substring(idx + 4);
		}
		else {
			throw new RuntimeException("pkg library not found: " + name);
		}
		return new URI(url);
	}
	
	
	private static class RJClassLoader extends URLClassLoader {
		
		
		public RJClassLoader(final URL[] urls, final ClassLoader parent) {
			super(urls, parent);
		}
		
		
		public Class<?> loadRJClass(final String name) throws ClassNotFoundException {
			return loadClass(name, true);
		}
		
		
	}
	
	
	public static void main(final String[] args) throws Exception {
		final ClassLoader parentClassLoader= ClassLoader.getSystemClassLoader();
		if (parentClassLoader == null) {
			throw new NullPointerException("systemClassLoader"); //$NON-NLS-1$
		}
		
		final List<URL> urls= new ArrayList<>();
		
		urls.add(getPkgLib("loader").toURL()); //$NON-NLS-1$
		
		final String rjClasspath= System.getProperty(RJ_CLASSPATH_PROPERTY_KEY);
		if (rjClasspath != null) {
			final String[] split= PATH_SPLITTER_PATTERN.split(rjClasspath);
			for (int i= 0; i < split.length; i++) {
				urls.add(new URL(split[i]));
			}
		}
		
		try {
			final RJClassLoader classLoader= new RJClassLoader(urls.toArray(new URL[urls.size()]),
					parentClassLoader );
			final Class<?> srvClass= classLoader.loadRJClass("GRSrv"); //$NON-NLS-1$
			final Method main= srvClass.getMethod("control", String[].class, ClassLoader.class); //$NON-NLS-1$
			main.invoke(main, new Object[] { args, classLoader });
		}
		catch (final ClassNotFoundException | NoClassDefFoundError e) {
			final StringBuilder sb= new StringBuilder();
			sb.append("RJ ClassPath/ClassLoader - URLs:"); //$NON-NLS-1$
			for (final URL url : urls) {
				sb.append("\n\t"); //$NON-NLS-1$
				sb.append(url);
			}
			System.err.println(sb);
			throw e;
		}
	}
	
}
