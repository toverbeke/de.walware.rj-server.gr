/*=============================================================================#
 # Copyright (c) 2018 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/


import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import javax.swing.UIManager;

import org.eclipse.statet.rj.RjException;
import org.eclipse.statet.rj.RjInitFailedException;
import org.eclipse.statet.rj.server.Server;
import org.eclipse.statet.rj.server.srv.CliUtil;
import org.eclipse.statet.rj.server.srv.RMIServerControl;
import org.eclipse.statet.rj.server.srv.ServerControl;
import org.eclipse.statet.rj.server.srv.engine.SrvEngine;
import org.eclipse.statet.rj.server.srv.engine.SrvEnginePluginExtension;
import org.eclipse.statet.rj.server.srv.engine.SrvEngineServer;
import org.eclipse.statet.rj.server.srvext.ServerRuntimePlugin;
import org.eclipse.statet.rj.server.util.LocalREnv;
import org.eclipse.statet.rj.server.util.ServerUtils;


/**
 * Main exec class for RJServ
 */
public class GRSrv {
	
	
	private static Logger getLogger() {
		return Logger.getLogger("de.walware.rj.server.gr.jri");
	}
	
	
	public static void main(final String[] args) throws RjException {
		control(args, ClassLoader.getSystemClassLoader());
	}
	
	public static void control(final String[] args, final ClassLoader classLoader) {
		final CliUtil cli= new CliUtil(args);
		final RMIServerControl serverControl;
		switch (cli.getCommand()) {
		
		case "start":
			serverControl= new RMIServerControl(cli.getName(), cli.getOptions());
			final SrvEngineServer srvEngineServer= serverControl.initServer();
			final SrvEngine engine= loadEngine(serverControl, srvEngineServer, classLoader);
			srvEngineServer.setEngine(engine);
			serverControl.start(srvEngineServer);
			return;
		
		case "clean":
			serverControl= new RMIServerControl(cli.getName(), cli.getOptions());
			serverControl.clean();
			return;
		}
	}
	
	
	private static SrvEngine loadEngine(final RMIServerControl control, final Server publicServer,
			final ClassLoader parentClassLoader) {
		final ClassLoader oldLoader= Thread.currentThread().getContextClassLoader();
		try {
			final LocalREnv rEnv= new LocalREnv();
			
			if (control.getOptions().containsKey("verbose")) {
				RJavaClassLoader.setDebug(1000);
			}
			final RJavaClassLoader loader= new RJavaClassLoader(rEnv, parentClassLoader);
			if (Boolean.parseBoolean(System.getProperty("org.eclipse.statet.rj.debug"))) {
				loader.setDefaultAssertionStatus(true);
			}
			
			// Add rj-srv to RJavaClassLoader
			// If this does not work, you can add in your command line to the rjava.class.path property
			final URI srvClasspath= RJSrv.getPkgLib("srv"); //$NON-NLS-1$
			loader.addClassPath(srvClasspath);
			
			final SrvEngine engine;
			try {
				Thread.currentThread().setContextClassLoader(loader);
				
				final Class<? extends SrvEngine> serverClazz= (Class<? extends SrvEngine>) loader.loadRJavaClass("de.walware.rj.internal.server.gr.jri.GRSrvEngine");
				
				engine= serverClazz.newInstance();
			}
			catch (final ClassNotFoundException e) {
				getLogger().log(Level.INFO,
						"Perhaps autodetection of 'rj-srv' classpath entry failed: " + 
						((srvClasspath != null) ? srvClasspath : "<not found>") );
				throw e;
			}
			
			final int[] rjVersion= ServerUtils.RJ_VERSION;
			final int[] implVersion= engine.getVersion();
			if (implVersion.length < 2
					|| implVersion[0] != rjVersion[0] || implVersion[1] != rjVersion[1]) {
				final StringBuilder sb= new StringBuilder();
				sb.append("The version of the loaded RJ server engine ");
				ServerUtils.prettyPrintVersion(implVersion, sb);
				sb.append(" is not compatible to RJ version ");
				ServerUtils.prettyPrintVersion(rjVersion, sb);
				sb.append(".");
				sb.append(" Make sure that the correct R package 'rj' is installed and in the R library path.");
				throw new RjInitFailedException(sb.toString());
			}
			
			final Map<String, Object> varargs= new HashMap<>();
			varargs.put("REnv", rEnv);
			varargs.put("ClassLoader", loader);
			engine.init(control, publicServer, varargs);
			
			
			final SrvEnginePluginExtension localServer= (SrvEnginePluginExtension) engine;
			
			// plugins
			final List<String> plugins= ServerUtils.getArgValueList(
					control.getOptions().get("plugins") );
			if (plugins.contains("awt")) {
				UIManager.put("ClassLoader", loader);
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				}
				catch (final Throwable e) {
				}
			}
			if (plugins.contains("swt")) {
				localServer.addPlugin((ServerRuntimePlugin) Class.forName("org.eclipse.statet.rj.server.e.srvext.SWTPlugin").newInstance());
			}
			return engine;
		}
		catch (final Throwable e) {
			final LogRecord record= new LogRecord(Level.SEVERE,
					"Initializing JRI/Rengine failed.");
			record.setThrown(e);
			getLogger().log(record);
			
			ServerControl.exit(ServerControl.EXIT_INIT_RENGINE_ERROR | 1);
			throw new RuntimeException();
		}
		finally {
			try {
				Thread.currentThread().setContextClassLoader(oldLoader);
			}
			catch (final Throwable e) { }
		}
	}
	
	
}
