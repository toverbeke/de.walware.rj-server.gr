/*=============================================================================#
 # Copyright (c) 2009, 2019 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import java.io.IOException;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.impl.RCharacter32Store;


public class JRICharacterDataImpl extends RCharacter32Store {
	
	
	public JRICharacterDataImpl(final String[] values) {
		super(values);
	}
	
	public JRICharacterDataImpl(final String[] values, final int length) {
		super(values, length);
	}
	
	public JRICharacterDataImpl(final RJIO io, final int l) throws IOException {
		super(io, l);
	}
	
	
	public String[] getJRIValueArray() {
		final int l= length();
		if (this.charValues.length == l) {
			return this.charValues;
		}
		final String[] array= new String[l];
		System.arraycopy(this.charValues, 0, array, 0, l);
		return array;
	}
	
}
