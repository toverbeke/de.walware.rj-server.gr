/*=============================================================================#
 # Copyright (c) 2009, 2019 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import java.io.IOException;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.impl.RCharacter32Store;
import org.eclipse.statet.rj.data.impl.RFactor32Store;


public class JRIFactorDataImpl extends RFactor32Store {
	
	
	public JRIFactorDataImpl(final int[] values, final boolean isOrdered, final String[] levelLabels) {
		super(values, isOrdered, levelLabels);
	}
	
	public JRIFactorDataImpl(final RJIO io, final int length) throws IOException {
		super(io, length);
	}
	
	
	@Override
	protected RCharacter32Store readLabels(final RJIO io, final int l) throws IOException {
		return new JRICharacterDataImpl(io, l);
	}
	
	
	public int[] getJRIValueArray() {
		final int l= length();
		if (this.codes.length == l) {
			return this.codes;
		}
		final int[] array= new int[l];
		System.arraycopy(this.codes, 0, array, 0, l);
		return array;
	}
	
	public String[] getJRILevelsArray() {
		return ((JRICharacterDataImpl) this.codeLabels).getJRIValueArray();
	}
	
}
