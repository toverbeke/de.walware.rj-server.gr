/*=============================================================================#
 # Copyright (c) 2009, 2019 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import java.io.IOException;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.impl.RInteger32Store;


public class JRIIntegerDataImpl extends RInteger32Store {
	
	
	public JRIIntegerDataImpl(final int[] values) {
		super(values);
	}
	
	public JRIIntegerDataImpl(final int[] values, final int length) {
		super(values, length);
	}
	
	public JRIIntegerDataImpl(final RJIO io, final int length) throws IOException {
		super(io, length);
	}
	
	
	public int[] getJRIValueArray() {
		final int l= length();
		if (this.intValues.length == l) {
			return this.intValues;
		}
		final int[] array= new int[l];
		System.arraycopy(this.intValues, 0, array, 0, l);
		return array;
	}
	
}
