/*=============================================================================#
 # Copyright (c) 2013, 2019 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import org.eclipse.statet.rj.data.impl.RListFix64Impl;


public class JRIListLongImpl extends RListFix64Impl {
	
	
	public JRIListLongImpl(final long length, final String className1) {
		super(length, (className1 != null) ? className1 : CLASSNAME_LIST);
	}
	
	
}
