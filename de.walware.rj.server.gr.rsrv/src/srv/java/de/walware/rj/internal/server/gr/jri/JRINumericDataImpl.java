/*=============================================================================#
 # Copyright (c) 2009, 2019 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import java.io.IOException;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.impl.RNumericB32Store;


public class JRINumericDataImpl extends RNumericB32Store {
	
	
	public JRINumericDataImpl(final double[] values) {
		super(values);
	}
	
	public JRINumericDataImpl(final RJIO io, final int length) throws IOException {
		super(io, length);
	}
	
	
	public double[] getJRIValueArray() {
		final int l= length();
		if (this.realValues.length == l) {
			return this.realValues;
		}
		final double[] array= new double[l];
		System.arraycopy(this.realValues, 0, array, 0, l);
		return array;
	}
	
}
