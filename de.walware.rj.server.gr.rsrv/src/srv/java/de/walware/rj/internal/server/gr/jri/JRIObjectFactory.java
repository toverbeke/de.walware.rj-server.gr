/*=============================================================================#
 # Copyright (c) 2008, 2019 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import java.io.IOException;

import org.eclipse.statet.rj.data.RCharacterStore;
import org.eclipse.statet.rj.data.RComplexStore;
import org.eclipse.statet.rj.data.RIntegerStore;
import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RLogicalStore;
import org.eclipse.statet.rj.data.RNumericStore;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RRawStore;
import org.eclipse.statet.rj.data.RStore;
import org.eclipse.statet.rj.data.impl.DefaultRObjectFactory;
import org.eclipse.statet.rj.data.impl.REnvironmentImpl;
import org.eclipse.statet.rj.data.impl.RFactorStructStore;
import org.eclipse.statet.rj.data.impl.RFunctionImpl;
import org.eclipse.statet.rj.data.impl.RLanguageImpl;
import org.eclipse.statet.rj.data.impl.RMissingImpl;
import org.eclipse.statet.rj.data.impl.RNullImpl;
import org.eclipse.statet.rj.data.impl.ROtherImpl;
import org.eclipse.statet.rj.data.impl.RPromiseImpl;
import org.eclipse.statet.rj.data.impl.RReferenceImpl;
import org.eclipse.statet.rj.data.impl.RS4ObjectImpl;
import org.eclipse.statet.rj.data.impl.RVectorImpl;


public class JRIObjectFactory extends DefaultRObjectFactory {
	
	
	@Override
	public RLogicalStore createLogiData(final boolean[] logiValues) {
		return new JRILogicalDataImpl(logiValues);
	}
	
	@Override
	public RIntegerStore createIntData(final int[] intValues) {
		return new JRIIntegerDataImpl(intValues);
	}
	
	@Override
	public RNumericStore createNumData(final double[] numValues) {
		return new JRINumericDataImpl(numValues);
	}
	
	@Override
	public RComplexStore createCplxData(final double[] reValues, final double[] imValues) {
		return new JRIComplexDataImpl(reValues, imValues);
	}
	
	@Override
	public RCharacterStore createCharData(final String[] charValues) {
		return new JRICharacterDataImpl(charValues);
	}
	
	@Override
	public RRawStore createRawData(final byte[] rawValues) {
		return new JRIRawDataImpl(rawValues);
	}
	
	
	@Override
	public RObject readObject(final RJIO io) throws IOException {
		final byte type= io.readByte();
		int options;
		switch (type) {
		case -1:
			return null;
		case RObject.TYPE_NULL:
			return RNullImpl.INSTANCE;
		case RObject.TYPE_VECTOR: {
			return new RVectorImpl(io, this); }
		case RObject.TYPE_ARRAY:
			return new JRIArrayImpl(io, this);
		case RObject.TYPE_LIST:
			options= io.readInt();
			return new JRIListImpl(io, this, options);
		case RObject.TYPE_DATAFRAME:
			return new JRIDataFrameImpl(io, this);
		case RObject.TYPE_ENVIRONMENT:
			return new REnvironmentImpl(io, this);
		case RObject.TYPE_LANGUAGE:
			return new RLanguageImpl(io, this);
		case RObject.TYPE_FUNCTION:
			return new RFunctionImpl(io, this);
		case RObject.TYPE_REFERENCE:
			return new RReferenceImpl(io, this);
		case RObject.TYPE_S4OBJECT:
			return new RS4ObjectImpl(io, this);
		case RObject.TYPE_OTHER:
			return new ROtherImpl(io, this);
		case RObject.TYPE_MISSING:
			return RMissingImpl.INSTANCE;
		case RObject.TYPE_PROMISE:
			if ((io.flags & F_WITH_DBG) != 0) {
				return new RPromiseImpl(io, this);
			}
			return RPromiseImpl.INSTANCE;
		default:
			throw new IOException("object type= " + type);
		}
	}
	
	@Override
	public RStore<?> readStore(final RJIO io, final long length) throws IOException {
		if ((io.flags & F_ONLY_STRUCT) == 0) {
			final byte storeType= io.readByte();
			switch (storeType) {
			case RStore.LOGICAL:
				return new JRILogicalDataImpl(io, (int) length);
			case RStore.INTEGER:
				return new JRIIntegerDataImpl(io, (int) length);
			case RStore.NUMERIC:
				return new JRINumericDataImpl(io, (int) length);
			case RStore.COMPLEX:
				return new JRIComplexDataImpl(io, (int)length);
			case RStore.CHARACTER:
				return new JRICharacterDataImpl(io, (int) length);
			case RStore.RAW:
				return new JRIRawDataImpl(io, (int) length);
			case RStore.FACTOR:
				return new JRIFactorDataImpl(io, (int) length);
			default:
				throw new IOException("store type= " + storeType);
			}
		}
		else {
			final byte storeType= io.readByte();
			switch (storeType) {
			case RStore.LOGICAL:
				return LOGI_STRUCT_DUMMY;
			case RStore.INTEGER:
				return INT_STRUCT_DUMMY;
			case RStore.NUMERIC:
				return NUM_STRUCT_DUMMY;
			case RStore.COMPLEX:
				return CPLX_STRUCT_DUMMY;
			case RStore.CHARACTER:
				return CHR_STRUCT_DUMMY;
			case RStore.RAW:
				return RAW_STRUCT_DUMMY;
			case RStore.FACTOR:
				return new RFactorStructStore(io.readBoolean(), io.readInt());
			default:
				throw new IOException("store type= " + storeType);
			}
		}
	}
	
	@Override
	public RStore<?> readNames(final RJIO io, final long length) throws IOException {
		final byte type= io.readByte();
		if (type == RStore.CHARACTER) {
			return new JRICharacterDataImpl(io, (int) length);
		}
		if (type == 0) {
			return null;
		}
		throw new IOException();
	}
	
}
