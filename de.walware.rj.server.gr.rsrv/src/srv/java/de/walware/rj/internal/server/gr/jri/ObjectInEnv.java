/*=============================================================================#
 # Copyright (c) 2014, 2019 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import org.eclipse.statet.jcommons.lang.NonNull;

import org.eclipse.statet.rj.server.rh.RhEnv;


public class ObjectInEnv {
	
	
	private final RhEnv env;
	private final long envP;
	
	private final String name;
	private final long nameStrP;
	
	
	public ObjectInEnv(final RhEnv env, final String name, final long nameStrP) {
		this.env= env;
		this.envP= env.handle.p;
		this.name= name;
		this.nameStrP= nameStrP;
	}
	
	
	public final RhEnv getEnv() {
		return this.env;
	}
	
	public final long getEnvP() {
		return this.envP;
	}
	
	public final String getName() {
		return this.name;
	}
	
	public final long getNameStrP() {
		return this.nameStrP;
	}
	
	
	@Override
	public @NonNull String toString() {
		return "`" + this.name + "` in " + this.env;
	}
	
}
